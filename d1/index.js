/*

npm init

npm install nodemon express mongoose

touch .gitignore - nodemodules

package.json - add "start" : "nodemon index"



*/

/*

Basic setup for express

const express = require('express');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.listen(port, () => console.log(`Local host: ${port}`))

*/


const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// mongodb+srv://admin:<password>@zuitt-bootcamp.nteyu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority


// Syntax:
	//mongoose.connect('<connection string>, {middlewares}')

mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.nteyu.mongodb.net/session30?retryWrites=true&w=majority', 
	{

		useNewUrlParser: true,
		useUnifiedTopology: true

	}

);

let db = mongoose.connection;

	//console.error.bind - print error in the browser and in the terminal
	db.on("error", console.error.bind(console, "Connection Error"))
	db.once('open', () => console.log(`Connected to MongoDB`))

// Mongoose Schema

const taskSchema = new mongoose.Schema({
	
	//define the name of Schema - taskSchema
	//new mongoose.Schema method - to make a schema
	//we will be needing the name of task and its status
	//each field will require a data type


	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});

const Task = mongoose.model('Task', taskSchema);
	// models use schema and they act as the middleman from the server to our database
	// Model can now be used to run commands for interacting with our database.
	// naming convention - name of model should be capitalized and singular form - 'Task'
	// second parameter is used to specify the schema of the documents that will be stored in the mongoDB collection

//Business Logic
	/*

		1. Add a functionality to check if there are duplicate tasks
			-if the task alreadt exist in the db, we return an error
			-if the task doesn't exists int the db, we add it in the db.
		2. The task data will be coming from the request body
		3.Create a new Task object with name field property.

	*/


app.post('/tasks', (req, res) => {

	// check any duplicate task
	//err shorthand for error

	Task.findOne({name: req.body.name}, (err, result) => {

		// if there are matches 
		if(result !== null && result.name == req.body.name){

			//will return this method
			return res.send('Duplicate task found.')
		} else {


			let newTask = new Task({

				name: req.body.name
			})

			//save method will accept a callback function which stores any errors in the first parameter and will store the newly saved document in the second parameter
			newTask.save((saveErr, savedTask) => {


				//if there are any errors, it will print error.
				if(saveErr){
					return console.error(saveErr)

				//no error found, return the status code and the message.
				} else {

					return res.status(201).send('New Task Created')
				}
			})
		}
	})
});

//Retrieving all tasks

app.get('/tasks', (req, res) => {

	Task.find({}, (err, result) => {

		if(err){
			return console.error(err);
		} else {

			return res.status(200).json({
				data: result
			})
		}
	})
})



// Activity

const usersSchema = new mongoose.Schema({

	username: String,
	password: String,
});

const Users = mongoose.model('Users', usersSchema);

app.post('/register', (req, res) => {
	
	Users.findOne({username: req.body.username}, (rror, resu) => {

		if(resu !== null && resu.username === req.body.username){

				return res.send(`Username already taken.`)

		} else {

			let newUsers = new Users({

				username: req.body.username,
				password: req.body.password
			})

			newUsers.save((saveRror, savedUsers) => {

				if(saveRror){
					return console.error(saveRror)
				} else {

					return res.status(201).send(`Your are now registered.`)
				}
			})
		}

	})


});

app.get('/users', (req, res) => {

	Users.find({}, (rror, resu) => {

		if(rror){ 

			return console.error(rror);
		} else {

			return res.status(201).json({

				data: resu
			})
		}
	})
})


	


app.listen(port, () => console.log(`Local host running at port:${port}`))